package no.uib.inf101.datastructure;

import java.util.List;

/**
 * Objects in a class extending CellColorCollection can assemble a list
 * of CellText objects through the getCells() method.
 */
public interface CellTextCollection {

  /**
   * Get a list containing the CellText objects in this collection
   *
   * @return a list of all CellText objects in this collection
   */
  List<CellText> getCells();

}
